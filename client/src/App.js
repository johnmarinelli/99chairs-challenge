import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import {FormControl, Row, Col, Button} from 'react-bootstrap';
import PropTypes from 'prop-types';
import './App.css';

class App extends Component {

  constructor (props) {
    super(props);
    this.state = {
      isUploading: false
    }

    this.upload = this.upload.bind(this);
  }

  upload (event) {
    const content = ReactDOM.findDOMNode(this.textInput).value;
    fetch('/api/firstnames', {
      method: 'post',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: {csv: content}
    })
    .then((res) => {
      console.log(res);
    })
    .catch((err) => {
      alert(err.message);
    });
  }

  render () {
    console.log(this.textInput);

    return (
      <div className="App">
        <Row>
          <Col sm={12}>
            <FormControl ref={(input) => { this.textInput = input; }} componentClass="textarea" bsSize="lg" />
            <Button bsStyle="primary" className="pull-left" onClick={this.upload}>
              <i className="fa fa-arrows-circle" />
              Upload file
            </Button>
          </Col>
        </Row>
        <Row>
        </Row>
      </div>
    );
  }
}

export default App;
