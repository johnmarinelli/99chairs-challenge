FROM node

WORKDIR /usr/app

COPY package.json . 
RUN npm install --quiet

EXPOSE 8080

COPY . . 
