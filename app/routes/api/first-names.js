let csvParse = require('csv-parse');

module.exports = (req, res) => {
  const parser = csvParse({columns: true});
  let objs = [];
  let errorFlag = false;

  /*
   * uses javascripts' stream API to parse csvs.
   */
  parser.on('readable', () => {
    let data;
    while (data = parser.read()) {
      objs.push(data);
    }
  });

  parser.on('error', (err) => {
    errorFlag = true;
    res.status(400).json({ 
      error: `There was an error processing your CSV.  Please check formatting.  Error: ${err.message}`
    });
  });

  parser.on('finish', () => {
    if (errorFlag) return;

    /*
     * builds an object that represents
     * the mapping between a name and its count
     */
    const nameMap = objs.reduce((acc, csvObj) => {
      const firstName = csvObj.first_name;

      if (undefined !== acc[firstName]) {
        acc[firstName] = acc[firstName] + 1;
      }
      else {
        acc[firstName] = 1;
      }

      return acc;

    }, {});

    /*
     * select only names with count > 1
     */
    const names = Object.keys(nameMap);
    const multipleNames = names.reduce((acc, name) => {
      if (nameMap[name] > 1 && undefined === acc[name]) {
        acc[name] = nameMap[name];
      }

      return acc;
    }, {});

    res.json(multipleNames);

  });

  parser.write(req.body.csv);
  parser.end();
}
