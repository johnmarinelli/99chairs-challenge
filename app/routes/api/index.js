let api = require('express').Router();
const firstNames = require('./first-names');

/*
 * task 1
 */
api.post('/firstnames', firstNames);
module.exports = api;
