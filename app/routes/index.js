let routes = require('express').Router();
let api = require('./api');

routes.use('/', api);
module.exports = routes;
