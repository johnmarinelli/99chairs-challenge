process.env.NODE_ENV = 'test';

let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../server');
let fs = require('fs');
let path = require('path');
let csvParse = require('csv-parse');
let csvStringify = require('csv-stringify');
let should = chai.should();
let expect = chai.expect;

chai.use(chaiHttp);

describe('api', () => {

  let csvFilepath = path.resolve(__dirname, 'data.csv');
  let csvRows = [];
  let csvString = '';

  beforeEach((done) => {
    const contents = fs.readFileSync(csvFilepath);

    csvParse(contents, {}, (err, rows) => {
      csvRows = rows;

      csvStringify(rows, (err, out) => {
        csvString = out;
        done();
      });
    });
  });

  /*
   * Test /api/firstnames (task 1)
   */
  it('should return an error given a bad csv string', (done) => {
    chai.request(server)
      .post('/api/firstnames')
      .send({ csv: '"header""' })
      .end((err, res) => {
        res.should.have.status(400);
        expect(res.body.error).to.match(/There was an error processing your CSV.  Please check formatting.  Error: /);
        done();
      });
  });

  it('should return a mapping between first names and count', (done) => {
    chai.request(server)
      .post('/api/firstnames')
      .send({ csv: csvString })
      .end((err, res) => {
        res.should.have.status(200);
        res.body.should.be.a('object');
        expect(res.body).to.deep.equal({john:2});
        done();
      });
  });

  afterEach((done) => {
    done();
  });
});
