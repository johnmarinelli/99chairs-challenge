# John Marinelli 99chairs Coding Challenge 
Hi!  This is the beginning of a Node/React app that processes CSVs. 
The API runs on port 8080 and has one endpoint - /api/firstnames.  
The React client runs on port 3000, but doesn't have any functionality; it only has a textarea and the beginnings of making API calls.  I spent too much time on the backend and ran out of time :|

I added MongoDB service and Redis service, but I am not actually using them in this app - I added them to show intent.
I didn't use Ruby for this because I have been enjoying React and JS for the past couple of months.  However, I am comfortable with Ruby.

## Initialization
Install dependencies on backend and client: `npm install; cd client; npm install`
In one terminal, run `docker-compose up --build`.  This will build the images and start the MongoDB, Redis, and backend API services. 
In another terminal, `cd client` and `npm start`.  This should start a Webpack dev server on port 3000.  
In yet another terminal you can run `npm run test` in the backend after docker has started everything.  

## Features
POST localhost:8080/api/firstnames with a json object whose shape looks like this: `{ csv: <CSV string>}`

